package comFiltros.clases;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
/**
 * Created by emr on 06/07/2017.
 */
public class PerMonitora {

    private String nombre;
    private String apellido;
    private Colonia[] listColonia;

    private List<Filtro> listadoFiltros;
    private int cantMonitoreos;
    private int cantColonias;

    private List<Familia> listadoFamilia;



    public PerMonitora() {
        listadoFamilia = new ArrayList<>();
        listadoFiltros = new ArrayList<>();
        listColonia = new Colonia[cantColonias];
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Colonia[] getListColonia() {
        return listColonia;
    }

    public void setListColonia(Colonia[] listColonia) {
        this.listColonia = listColonia;
    }

    public List<Filtro> getListadoFiltros() {
        return listadoFiltros;
    }

    public void setListadoFiltros(List<Filtro> listadoFiltros) {
        this.listadoFiltros = listadoFiltros;
    }

    public int getCantMonitoreos() {
        return cantMonitoreos;
    }

    public void setCantMonitoreos(int cantMonitoreos) {
        this.cantMonitoreos = cantMonitoreos;
    }

    public int getCantColonias() {
        return cantColonias;
    }

    public void setCantColonias(int cantColonias) {
        this.cantColonias = cantColonias;
    }

    public List<Familia> getListadoFamilia() {
        return listadoFamilia;
    }

    public void setListadoFamilia(List<Familia> listadoFamilia) {
        this.listadoFamilia = listadoFamilia;
    }
    public void agregarFamilia(Familia fm){
        listadoFamilia.add(fm);
    }
    public void mostrarFamilia(){
        for ( Familia fa: listadoFamilia) {
                System.out.println( "Apellidos: " + fa.getApellidos() );
                System.out.println("Hombres: " + fa.getNoHombres());
        }
    }
}

